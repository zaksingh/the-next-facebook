package blackjack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2006562
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BlackJack {
    
    Scanner scan = new Scanner(System.in);
    //initialize a full deck
    String[] ranks = new String[]{"A","2","3","4","5","6","7","8","9","10","J","Q","K"};
    String[] suits = new String[]{"Spades", "Hearts", "Clubs", "Diamonds"};
    int[] values = new int[]{10,2,3,4,5,6,7,8,9,10,10,10,10};
    Deck playDeck = new Deck(ranks,suits,values);
    //initiate dealer and player
    Player player = new Player(1000);
    Player dealer = new Player(0);
    int turnCounter = 0;
    
    public void newGame(){
        //have round until player ran out of money
        while(player.getBalance() > 0){
            turnCounter += 1;
            newRound();
        }
    }
    
    public void newRound(){
        System.out.println("-------------Round " + turnCounter + "-------------");
        if(playDeck.getDeckSize() < 5){
            //refills the deck if there are less than 5 cards left(not enough to start a game)
            refillDeck();
        }
        //empty the players' previous hands and deals two cards to player and dealer
        player.emptyHand();
        dealer.emptyHand();
        player.hand = playDeck.addToHand(player.hand, 2);
        dealer.hand = playDeck.addToHand(dealer.hand, 2);
        System.out.println("Input your bet:");
        //input this round's bet
        int bet = scan.nextInt();
        playerRound();
        dealerRound();
        System.out.println("Your hand's value is: " + player.getHandValue());
        System.out.println("The dealer's hand's value is: " + dealer.getHandValue());
        System.out.println("-------------Results-------------");
        //calculate if the player busted, if he didn't, whether his cards were better than the dealers
        if(player.getHandValue() > 21){
            if(dealer.getHandValue() <= 21){
                System.out.println("The dealer wins the pot");
                player.balance -= bet;
            }
            else{
                System.out.println("No one wins the pot");
            }
        }
        else{
            if(dealer.getHandValue() > player.getHandValue() && dealer.getHandValue() <= 21){
                System.out.println("The dealer wins the pot");
                player.balance -= bet;
            }
            else if(dealer.getHandValue() == player.getHandValue()){
                System.out.println("No one wins the pot");
            }
            else{
                System.out.println("You win the pot");
                player.balance += bet;
            }
        }
        System.out.println("Your balance is:" + player.getBalance());
    }
    
    public void playerRound(){
        System.out.println("-------------Player's Turn-------------");
        String input = "";
        while(input.equals("stay") == false){
            //the player draws a card until he inputs "stay"
            System.out.println("stay or hit?");
            input = scan.nextLine();
            if(input.equals("stay") == true){
                
            }
            else if(input.equals("hit") == true){
                player.hand = playDeck.addToHand(player.hand, 1);
                if(player.getHandValue() > 21){
                    input = "stay";
                    System.out.println("You busted");
                }
            }
            System.out.println("Your hand is: " + player.toString());
        }
    }
    
    public void dealerRound(){
        System.out.println("-------------Dealer's Turn-------------");
        //the dealer hits as long as his cards add up to less than 16, then stays
        System.out.println("The dealer's hand value is:" + dealer.toString());
        while(dealer.getHandValue() <= 16){
            dealer.hand = playDeck.addToHand(dealer.hand, 1);
            System.out.println("The dealer hits");
            System.out.println("The dealer's hand is: " + dealer.toString());
        }
        if(dealer.getHandValue() <= 21){
            System.out.println("The dealer stays");
        }
        else{
            System.out.println("The dealer busted");
        }
    }
    
    public void refillDeck(){
        //makes a new deck
        playDeck = new Deck(ranks,suits,values);
    }
}
