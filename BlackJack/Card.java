/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

/**
 *
 * @author Edwin Chen
 */

public class Card{
    private String rank, suit;
    private int value;
    
    public Card(String rank, String suit, int value){
        this.rank = rank;
        this.suit = suit;
        this.value = value;
    }
    
    public String getRank(){
        return rank;
    }
    
    public String getSuit(){
        return suit;
    }
    
    public int getValue(){
        return value;
    }
}
