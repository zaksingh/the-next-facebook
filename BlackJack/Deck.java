package blackjack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2006562
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Deck {
    Card tempCard = new Card(null, null, 0);
    ArrayList<Card> deck = new ArrayList<Card>();
    Random rng = new Random();
    
    public Deck(String[] ranks, String[] suits, int[] values){
        for(int x = suits.length - 1; x > 0; x--){
            for(int y = ranks.length - 1; y > 0; y--){
                tempCard = new Card(ranks[y], suits[x], values[y]);
                deck.add(tempCard);
            }
        }
        
    }
    
    public ArrayList<Card> getDeck(){
        return deck;
    }
    
    public int getDeckSize(){
        return deck.size();
    }
    
    public Card randomCard(){
        //finds a random card in the deck and returns it
        int temp = rng.nextInt(deck.size());
        Card tempCard = deck.get(temp);
        deck.remove(temp);
        return tempCard;
    }
    
    public ArrayList<Card> addToHand(ArrayList<Card> hand, int times){
        //runs the randomCard method for however times needed, add it to a temporary dummy of a hand then return the hand
        for(int x = times; x > 0; x--){
            hand.add(randomCard());
        }
        return hand;
    }
}
