/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

/**
 *
 * @author 2006562
 */
import java.io.IOException;
import java.util.ArrayList;

public class Player {
    ArrayList<Card> hand = new ArrayList<Card>(); 
    int balance;
    
    public Player(int balance){
        //this.hand = hand;
        this.balance = balance;
    }
    
    public ArrayList<Card> getHand(){
        return hand;
    }
    
    public int getBalance(){
        return balance;
    }
    
    public int getHandValue(){
        int value = 0;
        ArrayList<Card> tempHand = getHand();
        for(int x = tempHand.size() - 1; x > 0; x--){
            value += tempHand.get(x).getValue();
        }
        //Ace can eit
        for(int x = tempHand.size() - 1; x > 0; x--){
            if(value <= 21){
                break;
            }
            else if(tempHand.get(x).getRank().equals("A") == true){
                value -= 9;
            }
        }
        return value;
    }
    
    public void draw(Card c){
        hand.add(c);
    }
    
    public void emptyHand(){
        hand = new ArrayList<Card>();
    }
    
    public String toString(){
        String temp = "";
        for(int x = hand.size() - 1; x > 0; x--){
            temp = temp + hand.get(x).getRank() + " of " + hand.get(x).getSuit() + ", ";
        }
        temp += ".";
        return temp;
    }
}
